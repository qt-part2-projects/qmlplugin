#pragma once

#include <QQmlExtensionPlugin>

class FileioPlugin : public QQmlExtensionPlugin
{
  Q_OBJECT
  Q_PLUGIN_METADATA(IID "mephi.example.io")

public:
  void registerTypes(const char *uri);
};
