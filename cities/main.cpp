#include <QtGui/QGuiApplication>
#include <QtQml/QQmlApplicationEngine>
#include <QDebug>

int main(int argc, char *argv[])
{
  QGuiApplication app(argc, argv);

  QQmlApplicationEngine engine;
  qDebug()<< "importPathList: " << engine.importPathList();
  engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

  return app.exec();
}
